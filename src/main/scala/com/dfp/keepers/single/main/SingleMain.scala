package com.dfp.keepers.single.main

import com.dfp.keepers.population.Human
import com.dfp.keepers.population.Human._
import com.dfp.keepers.population.engine.PopulationSummary
import com.dfp.keepers.population.engine.PopulationEngine
import scala.io.StdIn
import com.dfp.keepers.main.Constants._

object SingleMain extends App {
  
  def reportYear(report: Seq[PopulationSummary]) {
    println("? ('exit' to end):")
    val input = StdIn.readLine()
    if (input == "exit")
      System.exit(0)
    else {
      try {
        val i = input.toInt
        println(report((report.size - 1) - i))
        reportYear(report)
        
      } catch {
        case _: NumberFormatException => {
          println(input + " could not be formatted to a number. Please enter a year to view the report for.")
          reportYear(report)
        }
        case _: IndexOutOfBoundsException => {
          println(input + " is not within the bounds of the population generated. Pick a year between 0 and " 
              + (report.size - 1)
          )
          reportYear(report)
        }
      }
    }
  }
  
  def update(spanCount: Int, lastUpdate: Long, updateInterval: Long): Long = {
    val timestamp = System.currentTimeMillis()
    if ((timestamp - lastUpdate) >= updateInterval) {
      println(spanCount + " years calculated")
      timestamp
    } else lastUpdate
  }
  
  def iterate(span: Int, 
      acc: Seq[PopulationSummary], 
      population: Seq[Human], 
      engine: PopulationEngine, 
      lastUpdate: Long, 
      updateInterval: Long
  ): Seq[PopulationSummary] = {
    if(span == 0) acc
    else {
      val report = engine.iterate(population)()
      //be sure to include any eligible males or eligible females that were not included in the population
//      println((report.eligibleMales ++ report.eligibleFemales))
      val nextGeneration = report.eligibleMales ++: report.eligibleFemales ++: report.population
//      println("span = " + span + " population = " + report.count)
      iterate(
          span - 1, 
          PopulationSummary(report) +: acc, 
          nextGeneration, 
          engine, 
          update(acc.size, lastUpdate, updateInterval),
          updateInterval
      )
    }
  }
  
  override def main(args: Array[String]) {
    try {
      val span = if(args.size > 0) args(0).toInt else DEFAULT_SPAN
      
       val firstWoman = Woman(100, 0)
       val firstMan = Husband(100, firstWoman)
  
       val engine = new PopulationEngine(DEFAULT_SEED)
  
       println("Calculating Populations, this could take some time..." + span)
       val startTime = System.currentTimeMillis()
       
       val generations = iterate(span, Seq(), Seq(firstMan), engine, System.currentTimeMillis(), 500)
       
       println("Calculation completed in " + (System.currentTimeMillis() - startTime))
       println("Calculation complete. Final population is " + generations(0).count + ". Enter a year you would like to "
           + "see a report for, press exit when done")
       
       reportYear(generations)
       
    } catch {
      case e: NumberFormatException => {
        println(args(0) + " can not be cast to an number. Please input the number of years you would like to generate "
            + "population for."
        )
        System.exit(1)
      }
      case e: IndexOutOfBoundsException => {
        println("Number of years to generate population for must be entered")
        System.exit(1)
      }
    }
  }
}
package com.dfp.keepers.population

//class Values {
//  //all values in years unless specified otherwise
//  val LIFE_SPAN = 900
//  val MATE_AGE = 15
//  val CHILD_SPACING = 3
//}

case class Values(lifeSpan: Int, mateAge: Int, childSpacing: Int, barren: Int) {
  assert(mateAge < lifeSpan)
  assert(mateAge < barren)
  assert(childSpacing < lifeSpan - mateAge)
  assert(barren < lifeSpan)
}

object Values {
  val DEFAULT_VALUES = Values(900, 15, 3, 600)
}

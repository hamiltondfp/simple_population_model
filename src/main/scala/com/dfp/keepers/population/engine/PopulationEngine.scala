package com.dfp.keepers.population.engine

import com.dfp.keepers.population.Values
import Values._
import scala.util.Random
import com.dfp.keepers.population.Human._
import com.dfp.keepers.population.Human

class PopulationEngine(seed: Int, values: Values = DEFAULT_VALUES)  {
  
  val random = new Random(seed)
  
  def iterate(population: Seq[Human])(populationReport: PopulationReport = PopulationReport()): PopulationReport = {
    population.foldRight(populationReport) {
      case (man @ Man(age), report) => {
        if(dead(man)) report.copy(deaths = report.deaths + 1)
        else if(age >= values.mateAge) {
          if(!report.eligibleFemales.isEmpty) {
            report.copy(
                count = report.count + 1,
                population = Husband(age + 1, report.eligibleFemales.head) +: report.population, 
                eligibleFemales = report.eligibleFemales.tail
            )
          }
          else report.copy(count = report.count + 1, eligibleMales = Man(age + 1) +: report.eligibleMales)
        }
        else report.copy(count = report.count + 1, population = Man(age + 1) +: report.population)
      }
      case (woman @ Woman(age, ageOfLastBirth), report) => {
        if(dead(woman)) report.copy(deaths = report.deaths + 1)
        else if(age >= values.mateAge && age < values.barren) {
          if(!report.eligibleMales.isEmpty) {
            report.copy(
                count = report.count + 1,
                population = Husband(
                    report.eligibleMales.head.age, 
                    Woman(age + 1, ageOfLastBirth)
                ) +: report.population,
                eligibleMales = report.eligibleMales.tail  
            )
          } else 
            report.copy(
                count = report.count + 1, 
                eligibleFemales = Woman(age + 1, ageOfLastBirth) +: report.eligibleFemales
            )
        }
        else 
          report.copy(
              count = report.count + 1, 
              population = Woman(age + 1, ageOfLastBirth) +: report.population
          )
      }
      case (man @ Husband(age, woman), report) => {
        if(dead(man)) 
          report.copy(
              deaths = report.deaths + 1, 
              count = report.count + 1, 
              population = Woman(woman.age + 1, woman.ageOfLastBirth) +: report.population
          )
        else if (dead(woman)) 
          report.copy(
              deaths = report.deaths + 1, 
              count = report.count + 1, 
              population = Man(age + 1) +: report.population
          )
        else if (woman.age < values.barren && (woman.age - woman.ageOfLastBirth) >= values.childSpacing) {
          report.copy(
              births = report.births + 1,
              count = report.count + 3,
              population = birth(man) +: Husband(age + 1, Woman(woman.age + 1, woman.age)) +: report.population
          )
        }
        else 
          report.copy(
              count = report.count + 2, 
              population = Husband(age + 1, Woman(woman.age + 1, woman.ageOfLastBirth)) +: report.population
          )
      }
    }
  }
  
  def birth(h: Husband): Human = if(h.age % 2 == 0) Man(1) else Woman(1, 0)//if(Random.nextBoolean) Woman(1, 0) else Man(1)
  
  def dead(h: Human) = h.age >= values.lifeSpan
}


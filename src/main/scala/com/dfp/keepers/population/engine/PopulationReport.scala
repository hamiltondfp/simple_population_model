package com.dfp.keepers.population.engine

import com.dfp.keepers.population.Human
import Human._

case class PopulationReport(
    births: Int = 0, 
    deaths: Int = 0, 
    count: Int = 0, 
    population: Seq[Human] = Seq(), 
    eligibleMales: Seq[Man] = Seq(), 
    eligibleFemales: Seq[Woman] = Seq()
) 

case class PopulationSummary(births: Int, deaths: Int, count: Int) {
  override def toString = {
    s"""totalPopulation = $count,
      |births = $births,
      |birth rate = ${births.doubleValue() / count.doubleValue},
      |deaths = $deaths,
      |death rate = ${deaths.doubleValue / count.doubleValue},
    """.stripMargin
  }
}

object PopulationSummary {
  def apply(report: PopulationReport): PopulationSummary = PopulationSummary(report.births, report.deaths, report.count)
}
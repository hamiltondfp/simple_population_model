package com.dfp.keepers.population.actor

import akka.actor.Actor
import com.dfp.keepers.population.Human
import Human._
import MatchMaker._
import PopulationIteration._
import akka.actor.ActorRef
import com.dfp.keepers.population.actor.PopulationWorker.Count
import akka.actor.Props


class MatchMaker(engine: ActorRef) extends Actor {
  
  def receive = active(Seq(), Seq())
   
  def active(males: Seq[Man], females: Seq[Woman]): Receive = {
    case RequestSingles => {
      sender ! Singles(males ++ females)
      context stop self
    }
    case MakeMeAMatch(man: Man) => {
      if(!females.isEmpty) {
        engine ! Count(Husband(man.age, females.head), false)
        context.become(active(males, females.tail))
      }
      else context.become(active(man +: males, females))
    }
    case MakeMeAMatch(woman: Woman) => {
      if(!males.isEmpty) {
        engine ! Count(Husband(males.head.age, woman), false)
        context.become(active(males.tail, females))
      }
      else context.become(active(males, woman +: females))
    } case any: Any => println("******************FAIL!!!!!*********************")
  }
}

object MatchMaker {
  def props(iteration: ActorRef) = Props(new MatchMaker(iteration))
  
  case object RequestSingles
  
  case class Singles(human: Seq[Human])
  case class MakeMeAMatch(human: Human)
  
}
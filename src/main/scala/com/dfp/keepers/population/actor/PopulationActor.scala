package com.dfp.keepers.population.actor

import akka.actor.Actor
import com.dfp.keepers.population.Human
import PopulationActor._
import PopulationIteration._
import akka.actor.ActorRef
import akka.actor.Props
import com.dfp.keepers.main.Constants._
import Workers._

class PopulationActor extends Actor{
  
  def update(spanCount: Int, lastUpdate: Long): Long = {
    val timestamp = System.currentTimeMillis()
    if ((timestamp - lastUpdate) >= UPDATE_INTERVAL) {
      println(spanCount + " years calculated")
      timestamp
    } else lastUpdate
  }
  
  def receive = {
    case StartSimulation(span, initial) => {
      context.actorOf(Workers.props, "workers") ! Initialize
      context.become(initialize(sender, span, initial), true)
    }
  }
  
  def initialize(client: ActorRef, span: Int, initial: Seq[Human]): Receive = {
    case Initialized => {
      context.actorOf(
          PopulationIteration.props(
              BATCH_SIZE, 
              Iteration(span, Accumulator(Seq()), initial, System.currentTimeMillis()), 
              self
          )
      )
      context.become(process(client), true)
    }
  }
  
  def process(client: ActorRef): Receive = {
    case Iteration(span, acc, population, lastUpdate) => {
      if(span == 1) {
        println("span = 0")
        client ! acc
      }
      else { 
//        println("span == " + span)
        context.actorOf(
            PopulationIteration.props(
                BATCH_SIZE, 
                Iteration(span - 1, acc, population, update(span, lastUpdate)), 
                self
            )//.withDispatcher("iterationDispatcher")
        )
      }
    }
  }
}

object PopulationActor {
  def props = Props(new PopulationActor)
  
  case class StartSimulation(span: Int, population: Seq[Human])
}
package com.dfp.keepers.population.actor

import akka.actor.Actor
import akka.actor.ActorRef
import com.dfp.keepers.population.Human
import Human._
import PopulationWorker._
import com.dfp.keepers.population.Values
import Values._
import scala.util.Random
import com.dfp.keepers.population.actor.PopulationIteration._
import akka.actor.Props
import MatchMaker._

class PopulationWorker(values: Values = DEFAULT_VALUES) extends Actor {
  def receive = readyForWork
  
  def readyForWork: Receive = {
    case Start => sender ! Ready
    case Work(human, matchMaker) => process(sender, matchMaker)(human)
//    case Done => //just ignore, we'll continue waiting for work
  }
  
  def process(sender: ActorRef, matchMaker: ActorRef): PartialFunction[Human, Unit] = {
    case h @ Husband(_, Woman(age, cb)) if(dead(h)) => {
      sender ! Death
      sender ! Count(Woman(age + 1, cb), false)
    }
    case Husband(age, wife) if(dead(wife)) => {
      sender ! Death
      sender ! Count(Man(age + 1), false)
    }
    case h if(dead(h)) => sender ! Death
    case Man(age) if(age >= values.mateAge) => {
      matchMaker ! MakeMeAMatch(Man(age + 1))
      sender ! Continue
    }
    case Woman(age, cb) if(age >= values.mateAge && age < values.barren) => {
      matchMaker ! MakeMeAMatch(Woman(age + 1, cb)) 
      sender ! Continue
    }
    case h @ Husband(mAge, Woman(wAge, cb)) if((wAge - cb) >= values.childSpacing && wAge < values.barren) => {
      sender ! Birth(Husband(mAge + 1, Woman(wAge + 1, wAge)), birth(h))
    }
    case h @ Husband(mAge, Woman(wAge, cb)) => {
      sender ! Count(Husband(mAge + 1, Woman(wAge + 1, cb)))
    }
    case Man(age) => sender ! Count(Man(age + 1))
    case Woman(age, cb) => sender ! Count(Woman(age + 1, cb))
    case any: Any => println("received something weird: " + any)
  }
  
  def dead(human: Human) = human.age >= values.lifeSpan
  
  def birth(h: Husband) = if(h.age % 2 == 0) Man(1) else Woman(1, 0)//if(Random.nextBoolean) Man(1) else Woman(1, 0)
}

object PopulationWorker {
  def props(values: Values = DEFAULT_VALUES) = Props(new PopulationWorker(values))
  case class Work(human: Human, matchMaker: ActorRef)
  case object Death
  case class Birth(parents: Husband, child: Human)
  case class Count(human: Human, sendMore: Boolean = true)
}
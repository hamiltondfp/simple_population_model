package com.dfp.keepers.population.actor

import akka.actor.Actor
import com.dfp.keepers.population.Human
import Human._
import PopulationIteration._
import akka.actor.ActorSelection
import akka.actor.Props
import com.dfp.keepers.population.engine.PopulationSummary
import com.dfp.keepers.population.engine.PopulationReport
import com.dfp.keepers.main.Constants._
import com.dfp.keepers.population.actor.MatchMaker.RequestSingles
import akka.actor.ActorRef
import com.dfp.keepers.population.actor.MatchMaker.Singles
import PopulationWorker._

class PopulationIteration(
    batchSize: Int, 
    iteration: Iteration, 
    populationActor: ActorRef, 
    matchMakerProps: ActorRef => Props = MatchMaker.props(_)
) extends Actor {
  
  val matchMaker = context.actorOf(matchMakerProps(self))
  
  override def preStart = {
    context.system.actorSelection("/user/population/workers/*") ! Start
  }
  
  def receive = statefulReceive(State(population = iteration.population))
  
  def done(currentState: State, changeReport: PopulationReport => PopulationReport, sendMore: Boolean = true) {
//    if(sendMore) sender ! Done
    if(sendMore && currentState.workers <= 1) {
      matchMaker ! RequestSingles
      context.become(finish(currentState.copy(report = changeReport(currentState.report))), true)
    }
    else {
      context.become(
          statefulReceive(
              currentState.copy(workers = if(sendMore) currentState.workers - 1 else currentState.workers, report = changeReport(currentState.report))
          ), 
          true
      )
    }
  }
    
  def statefulReceive(state: State): Receive = {
    case Ready if(state.population.isEmpty) => {/*don't do anything*/}
    case Ready => {
      sender ! Work(state.population.head, matchMaker)
      context.become(statefulReceive(State(state.workers + 1, state.population.tail, state.report)), true)
    }
    case Continue if(state.population.isEmpty) => {
      done(state, identity)
    }
    case Continue => {
      sender ! Work(state.population.head, matchMaker)
      context.become(statefulReceive(state.copy(population = state.population.tail)), true)
    }
    case Death if(state.population.isEmpty) => {
      done(state, death)
    }
    case Death => {
      context.become(statefulReceive(state.copy(population = state.population.tail)), true)
      sender ! Work(state.population.head, matchMaker)
    }
    case Birth(parents, child) if (state.population.isEmpty) => {
      done(state, birth(parents, child, _))
    }
    case Birth(parents, child) => {
      context.become(
          statefulReceive(state.copy(population = state.population.tail, report = birth(parents, child, state.report))), 
          true
      )
      sender ! Work(state.population.head, matchMaker)
    }
    case Count(h: Husband, sendMore) if(state.population.isEmpty) => {
      done(state, count(2, h, _), sendMore)
    }
    case Count(h: Husband, sendMore) => {
      context.become(
          statefulReceive(
              state.copy(
                  population = if(sendMore) state.population.tail else state.population, 
                  report = count(2, h, state.report)
              )
          ), 
          true
      )
      if(sendMore) sender ! Work(state.population.head, matchMaker)
    }
    case Count(human, sendMore) if(state.population.isEmpty) => {
      done(state, count(1, human, _), sendMore)
    }
    case Count(human, sendMore) => {
      context.become(
          statefulReceive(
              state.copy(
                  population = if(sendMore) state.population.tail else state.population, 
                  report = count(1, human, state.report)
              )
          ), 
          true
      )
      if(sendMore) sender ! Work(state.population.head, matchMaker)
    }
  }
  
  def finish(state: State): Receive = {
    case Singles(humans) => {
      val report = state.report.copy(
          count = state.report.count + humans.size, 
          population = humans ++: state.report.population
      )
      populationActor ! Iteration(
          iteration.span, 
          Accumulator(PopulationSummary(report) +: iteration.acc.summaries), 
          report.population,
          iteration.lastUpdate
      )
      context stop self
    }
    case Count(h: Husband, _) => {
      context.become(finish(state.copy(report = count(2, h, state.report))), true)
    }
    case any: Any => //println("*********************************************LATE**************************" + any)
  }
  
  def death(report: PopulationReport) = report.copy(deaths = report.deaths + 1)
  
  def birth(parents: Husband, child: Human, report: PopulationReport) = {
    report.copy(
        births = report.births + 1, 
        count = report.count + 3, 
        population = (parents +: child +: report.population)
    )
  }
    
  def count(count: Int, human: Human, report: PopulationReport) = 
    report.copy(count = report.count + count, population = human +: report.population)
}

object PopulationIteration {
  
  /*
   * Need to enforce at least 2. If you have only one worker, it is possible to create an infinite loop between this 
   * actor's Scan messages and the Worker Actor's Ready Message.
   */
  val SCAN_COUNT = scala.math.max(WORKER_COUNT / 4, 2) * 100
  val APPROACHING_DONE = scala.math.max((SCAN_COUNT / 4) * 3, 4)
  
  def props(batchSize: Int, iteration: Iteration, populationActor: ActorRef) = 
    Props(new PopulationIteration(batchSize, iteration, populationActor: ActorRef))
    
  def props(batchSize: Int, iteration: Iteration, populationActor: ActorRef, matchMakerProps: ActorRef => Props) = 
    Props(new PopulationIteration(batchSize, iteration, populationActor, matchMakerProps))
  
  case object Start
  case object Ready
  case object Continue
//  case object Done
  
  case class Accumulator(summaries: Seq[PopulationSummary])
  
  case class Iteration(
      span: Int, 
      acc: Accumulator, 
      population: Seq[Human], 
      lastUpdate: Long
  )
  
//  case object Scan
  
  case class State(workers: Int = 0, population: Seq[Human] = Seq(), report: PopulationReport = PopulationReport())
  
}
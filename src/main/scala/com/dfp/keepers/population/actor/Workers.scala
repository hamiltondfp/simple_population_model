package com.dfp.keepers.population.actor

import akka.actor.Actor
import akka.actor.Props
import com.dfp.keepers.main.Constants._
import Workers._

class Workers extends Actor {
  
  def receive: Receive = {
    case Initialize => {
      for (i <- (0 until WORKER_COUNT)) {context.actorOf(PopulationWorker.props())}
      sender ! Initialized
    }
  }
}

object Workers {
  def props = Props(new Workers())
  
  case object Initialize
  case object Initialized
}
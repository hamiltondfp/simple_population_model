package com.dfp.keepers.population

sealed trait Human {
  val age: Int
}

object Human {
  case class Man(age: Int) extends Human
  
  case class Husband(age: Int, partner: Woman) extends Human
  
  case class Woman(age: Int, ageOfLastBirth: Int) extends Human
}
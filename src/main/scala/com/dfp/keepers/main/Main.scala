package com.dfp.keepers.main

import com.dfp.keepers.population.Human
import Human._
import com.dfp.keepers.population.engine.PopulationSummary
import scala.io.StdIn
import Constants._
import akka.actor.ActorSystem
import com.dfp.keepers.population.actor.PopulationIteration
import PopulationIteration._
import akka.pattern.ask
import scala.concurrent.duration._
import akka.util.Timeout
import scala.concurrent.Future
import com.dfp.keepers.population.engine.PopulationReport
import com.dfp.keepers.population.actor.PopulationActor
import com.dfp.keepers.population.actor.PopulationActor.StartSimulation
import com.dfp.keepers.population.actor.PopulationWorker

object Constants {
  val BATCH_SIZE = 1
  val DEFAULT_SEED = 100
  val DEFAULT_SPAN = 200
  val UPDATE_INTERVAL = 500
  val WORKER_COUNT = 20
  
  implicit val timeout: Timeout = 5 hours
}

object Main extends App {
  
  def reportYear(report: Seq[PopulationSummary]) {
    println("? ('exit' to end):")
    val input = StdIn.readLine()
    if (input == "exit") {
      println("goodbye")
      //System.exit(1)
    }
    else {
      try {
        val i = input.toInt
        println(report((report.size - 1) - i))
        reportYear(report)
        
      } catch {
        case _: NumberFormatException => {
          println(input + " could not be formatted to a number. Please enter a year to view the report for.")
          reportYear(report)
        }
        case _: IndexOutOfBoundsException => {
          println(input + " is not within the bounds of the population generated. Pick a year between 0 and " 
              + (report.size - 1)
          )
          reportYear(report)
        }
      }
    }
  }
  
  override def main(args: Array[String]) {
    try {
      val span = if(args.size > 0) args(0).toInt else DEFAULT_SPAN
      
      val system = ActorSystem("population")
      implicit val executionContext = system.dispatcher
      
       val firstWoman = Woman(100, 0)
       val firstMan = Husband(100, firstWoman)
  
       println("Calculating Populations, this could take some time..." + span)
       val startTime = System.currentTimeMillis()
       
       val future = (system.actorOf(PopulationActor.props, "population") ? StartSimulation(span, Seq(firstMan)))
       (future.map {
         case Accumulator(summaries) => {
           println("Calculation completed in " + (System.currentTimeMillis() - startTime))
           println("Calculation complete. Final population is " + summaries(0).count 
               + ". Enter a year you would like to see a report for, press exit when done"
           )
           reportYear(summaries)
         }
         case any: Any => println("received " + any)
      }).onComplete {
        case _ => {
          system.terminate().onComplete{
            case _ => sys.exit(0)
          }
        }
      }
    } catch {
      case e: NumberFormatException => {
        println(args(0) + " can not be cast to an number. Please input the number of years you would like to generate "
            + "population for."
        )
        sys.exit(1)
      }
      case e: IndexOutOfBoundsException => {
        println("Number of years to generate population for must be entered")
        sys.exit(1)
      }
    }
  }
}
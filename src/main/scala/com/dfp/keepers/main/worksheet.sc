package com.dfp.keepers.main

object worksheet {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val n = 2                                       //> n  : Int = 2
  val d = n / 4                                   //> d  : Int = 0
  val ad = (n / 4) * 3                            //> ad  : Int = 0
}
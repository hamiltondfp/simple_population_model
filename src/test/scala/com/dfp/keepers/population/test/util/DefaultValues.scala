package com.dfp.keepers.population.test.util

import com.dfp.keepers.population.Values
import com.dfp.keepers.population.Human._
import com.dfp.keepers.population.actor.PopulationIteration.Accumulator
import com.dfp.keepers.population.actor.PopulationIteration.Iteration

trait DefaultValues {
  val testValues = Values(100, 15, 3, 60)
  
  val babyMan = Man(0)
  val babyWoman = Woman(0, 0)
  
  val manOfAge = Man(testValues.mateAge)
  val womanOfAge = Woman(testValues.mateAge, 0)
  val barrenWoman = Woman(testValues.barren, 0)
  
  val dyingMan = Man(testValues.lifeSpan)
  val dyingWoman = Woman(testValues.lifeSpan, 0)
  val dyingHusband = Husband(testValues.lifeSpan, womanOfAge)
  val dyingWife = Husband(testValues.mateAge, dyingWoman)
  
  val readyForBaby = Husband(testValues.mateAge, Woman(testValues.mateAge, 0))
  val notReadyForBaby = Husband(testValues.mateAge, Woman(testValues.mateAge, testValues.mateAge))
  val barrenCouple = Husband(testValues.barren, Woman(testValues.barren, 0))
  
  val iteration = Iteration(1, Accumulator(Seq()), Seq(manOfAge), System.currentTimeMillis)
  val emptyIteration = iteration.copy(population = Seq())
}
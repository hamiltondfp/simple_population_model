package com.dfp.keepers.population.actors

import akka.testkit.TestKit
import akka.actor.ActorSystem
import org.scalatest.FunSuiteLike
import org.scalatest.Suite
import com.dfp.keepers.population.test.util.DefaultValues
import akka.testkit.ImplicitSender
import com.dfp.keepers.population.actor.MatchMaker
import MatchMaker._
import akka.testkit.TestProbe
import com.dfp.keepers.population.actor.PopulationWorker._
import com.dfp.keepers.population.Human._

class MatchMakerTest 
    extends TestKit(ActorSystem("population")) with FunSuiteLike with Suite with DefaultValues with ImplicitSender {
  
  trait DefaultTest {
    val actor = system.actorOf(MatchMaker.props(self))
    
    val deathProbe = TestProbe()
    deathProbe watch actor
    
    def finish {
      system stop actor
      deathProbe.expectTerminated(actor)
    }
  }
  
  test("If there are no males or females that are eligible for mating, RequestSingles will return an empty list") {
    new DefaultTest {
      actor ! RequestSingles
      
      expectMsg(Singles(Nil))
      
      finish
    }
  }
  
  test("If a Man is sent to the MatchMaker and there are no eligible females for mating, the Man will be added to a "
      + "list of eligible males which can be retrieved with RequestSingles") {
    new DefaultTest {
      actor ! MakeMeAMatch(manOfAge)
      
      actor ! RequestSingles
      
      expectMsg(Singles(manOfAge :: Nil))
      
      finish
    }
  }
  
  test("If a Woman is sent to the MatchMaker and there are no eligible males for mating, the Woman will be added to a "
      + "list of eligible females which can be retrieved with RequestSingles") {
    new DefaultTest {
      actor ! MakeMeAMatch(womanOfAge)
      
      actor ! RequestSingles
      
      expectMsg(Singles(womanOfAge :: Nil))
      
      finish
    }
  }
  
  test("If a Man and a Woman are both sent to the MatchMaker, they will be matched and added back to the population "
      + "and a RequestSingles message wil again not return anything") {
    new DefaultTest {
      actor ! MakeMeAMatch(manOfAge)
      actor ! MakeMeAMatch(womanOfAge)
    
      expectMsg(Count(Husband(manOfAge.age, Woman(womanOfAge.age, womanOfAge.ageOfLastBirth))))
      
      actor ! RequestSingles
      
      expectMsg(Singles(Nil))
      
      finish
    }
  }
}
package com.dfp.keepers.population.actors

import org.scalatest.FunSuite
import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.FunSuiteLike
import org.scalatest.Suite
import com.dfp.keepers.population.test.util.DefaultValues
import com.dfp.keepers.population.actor.PopulationWorker
import akka.actor.Actor
import akka.actor.Props
import akka.testkit.ImplicitSender
import com.dfp.keepers.population.actor.PopulationIteration._
import scala.concurrent.duration._
import akka.testkit.TestProbe
import com.dfp.keepers.population.actor.PopulationWorker._
import com.dfp.keepers.population.actor.MatchMaker.MakeMeAMatch
import com.dfp.keepers.population.Human
import Human._

class PopulationWorkerTest 
    extends TestKit(ActorSystem("population")) with FunSuiteLike with Suite with DefaultValues with ImplicitSender {
  
  trait DefaultTest {
    val test = self
    
    val actor = system.actorOf(PopulationWorker.props(testValues))
    
    val deathProbe = TestProbe()
    
    deathProbe.watch(actor)
    
    def finish {
      system stop actor
      deathProbe.expectTerminated(actor)
    }
  }
  
  test("If the PopulationWorker is Idle when it receives a Start message, it will respond with Ready") {
    new DefaultTest {
      actor ! Start
      
      expectMsg(Ready)
      
      finish
    }
  }
  
  test("If the PopulationWorker is Idle when it receives a Work message, it will Process the message and switch to the "
      + "Working state, wich will ignore future requests to start") {
    new DefaultTest {
      actor ! Work(dyingMan, self)
      
      expectMsg(Death)
      
      actor ! Start
      
      expectNoMsg
      
      finish
    }
  }
  
//  test("If the PopulationWorker is working, and it receives a Done message, it will switch to an Idle state, where it "
//      + "will ignore further Done messages.") {
//    new DefaultTest {
//      actor ! Work(dyingMan, self)
//      expectMsg(Death)
//      
//      actor ! Done
//      //this should switch the context, but will not send a response
//      expectNoMsg
//      
//      //Best way to test that we are back in Idle mode is to send a start
//      actor ! Start
//      expectMsg(Ready)
//      
//      finish
//    }
//  }
  
  test("If a Husband has reached his life expectency, he will be counted dead and his wife will grow older and be "
      + "returned back to the population") {
    new DefaultTest {
      actor ! Work(dyingHusband, self)
      
      receiveN(2) foreach {
        case Death => assert(true)
        case Count(woman: Woman, false) => assert(woman.age === dyingHusband.partner.age + 1)
        case a: Any => fail("Expected Death or Woman, received: " + a)
      }
      
      finish
    }
  }
  
  test("If a Wife has reached her life expectency, she will be counted dead and her husband will grow older and be "
      + "returned back to the population") {
    new DefaultTest {
      actor ! Work(dyingWife, self)
      
      receiveN(2) foreach {
        case Death => assert(true)
        case Count(man: Man, false) => assert(man.age === dyingWife.age + 1)
        case a: Any => fail("Expected Death or Man, received: " + a)
      }
      
      finish
    }
  }
  
  test("If a Woman reaches her life expectency she will be counted as dead") {
    new DefaultTest {
      actor ! Work(dyingWoman, self)
      expectMsg(Death)
      finish
    }
  }
  
  test("If a Man of age is sent to the PopulationWorker, the Man will grow older and be forwarded to the MatchMaker. "
      + "Then a Continue message will be sent back to the sender notifying the PopulationIteration that this "
      + "PopulationWork is ready for more work") {
    new DefaultTest {
      actor ! Work(manOfAge, self)
      
      receiveN(2) foreach {
        case Continue => assert(true)
        case MakeMeAMatch(Man(age)) => assert(age === manOfAge.age + 1)
        case a: Any => fail("Expected Continue or MakeMeAMatch(Man), received: " + a)
      }
      finish
    }
  }
  
  test("If a Woman of age is sent to the PopulationWorker she will grow older, be forwarded to the MatchMaker, and a "
      + "Continue message will be sent back to the PopulationIteration informing it that the message has been handled "
      + "and this PopulationWorker is ready for more work.") {
    new DefaultTest {
      actor ! Work(womanOfAge, self)
      
      receiveN(2) foreach {
        case Continue => assert(true)
        case MakeMeAMatch(Woman(age, _)) => assert(age === womanOfAge.age + 1)
        case a: Any => fail("Expected Continue or MakeMeAMatch(Woman), received: " + a)
      }
      
      finish
    }
  }
  
  test("If a Woman of age is sent to the PopulationWorker, but she is barren, she will grow older but will not be sent "
      + "to the MatchMaker") {
    new DefaultTest {
      actor ! Work(barrenWoman, self)
      
      expectMsg(Count(Woman(barrenWoman.age + 1, 0)))
      
      finish
    }
  }
  
  test("If a Couple is sent to the PopulationWorker and the time since the Wife's last child is greater than the child "
      + "spacing period, the Husband and Wife will each increase in age, the Wife's age of last child birth will be "
      + "set to her current age and a new man or woman of age 0 will be added to the population") {
    new DefaultTest {
      actor ! Work(readyForBaby, self)
      
      receiveOne(3 seconds) match {
        case Birth(Husband(mAge, Woman(wAge, cb)), h: Human) => {
          assert(mAge === readyForBaby.age + 1)
          assert(wAge === readyForBaby.partner.age + 1)
          assert(cb === wAge)
          assert(h.age === 0)
        }
      }
      
      finish
    }
  }
  
  test("If a Couple is sent to the PopulationWorker and the time since the Wife's last child is greater than the child "
      + "spacing period, but the Wife is barren then the Husband and Wife will both increase in age without having "
      + "another child") {
    new DefaultTest {
      actor ! Work(barrenCouple, self)
      
      expectMsg(
          Count(Husband(barrenCouple.age + 1, Woman(barrenCouple.partner.age + 1, barrenCouple.partner.ageOfLastBirth)))
      )
      
      finish
    }
  }
  
  test("If a Couple is Sent to the PopulationWorker and the time since the Wife's last child is less than or equal to "
      + "the child spacing period, the Husband and Wife will both age without having another child") {
    new DefaultTest {
      actor ! Work(notReadyForBaby, self)
      
      expectMsg(
          Count(
              Husband(
                  notReadyForBaby.age + 1, 
                  Woman(notReadyForBaby.partner.age + 1, notReadyForBaby.partner.ageOfLastBirth)
              )
          )
      )
      
      finish
    }
  }
  
  test("If a Man is sent to the PopulationWorker and he is not of age, he will simply grow older and will not be sent "
      + "to the MatchMaker") {
    new DefaultTest {
      actor ! Work(babyMan, self)
      
      expectMsg(Count(Man(babyMan.age + 1)))
      
      finish
    }
  }
  
  test("If a Woman is sent to the PopulationWorker and she is not of age, she will simply grow older and will not be "
      + "sent to the MatchMaker") {
    new DefaultTest {
      actor ! Work(babyWoman, self)
      
      expectMsg(Count(Woman(babyWoman.age + 1, babyWoman.ageOfLastBirth)))
      
      finish
    }
  }
}
package com.dfp.keepers.population.actors

import akka.testkit.TestKit
import akka.actor.ActorSystem
import org.scalatest.FunSuiteLike
import org.scalatest.Suite
import com.dfp.keepers.population.test.util.DefaultValues
import akka.testkit.ImplicitSender
import com.dfp.keepers.population.actor.PopulationIteration
import PopulationIteration._
import akka.actor.ActorRef
import akka.actor.Actor
import akka.actor.Props
import akka.testkit.TestProbe
import com.dfp.keepers.population.actor.PopulationWorker._
import com.dfp.keepers.population.Human._
import scala.concurrent.duration._
import com.dfp.keepers.population.actor.MatchMaker._
import akka.testkit.TestActorRef
import com.dfp.keepers.population.engine.PopulationSummary

class PopulationIterationTest extends 
    TestKit(ActorSystem("population")) 
    with FunSuiteLike 
    with Suite 
    with DefaultValues {
  
  class ForwardActor(tRef: ActorRef) extends Actor {
    override def receive: Receive = {
      case any: Any => tRef ! any
    }
  }
  
  class MockMM extends Actor {
    override def receive = {
      case RequestSingles => sender ! Singles(Nil)
    }
  }
  
  class NoStartPopulationIteration(
      batchSize: Int, 
      iteration: Iteration, 
      populationActor: ActorRef, 
      mmProps: ActorRef => Props
   ) extends PopulationIteration(batchSize, iteration, populationActor, mmProps) {
    override def preStart {}
  }
  
  trait DefaultTest {
    val probe = TestProbe()    
    implicit val implicitSender = probe.ref
    
    val worker = TestProbe()
    
    val actor = define
    
    val deathProbe = TestProbe()
    deathProbe watch actor   
    
    def itToUse = iteration
    
    def mockMM = (ar: ActorRef) => Props(new MockMM())
    
    def define = system.actorOf(
        PopulationIteration.props(1, itToUse, probe.ref, mockMM)
    )
    
    def finish {
      system stop actor
      deathProbe.expectTerminated(actor)
    }
  }
  
//  trait NoScan extends DefaultTest {
//    override def define = system.actorOf(
//        Props(new NoStartPopulationIteration(1, itToUse, probe.ref, mockMM))
//    )
//  }
  
  test("PopulationIteration whose state containes some population to calculate will send Work to any Actor that sends "
      + "it a Ready message") {
    new DefaultTest {
      actor ! Ready
      
      probe.receiveOne(3 seconds) match {
        case Work(Man(age), _) => assert(age === iteration.population.head.age)
        case a: Any => fail("Expected Work(Man, ActorRef), received : " + a)
      }
      
      finish
    }
  }
  
  test("PopulationIteration will kill itself after some time with zero population") {
    new DefaultTest {
      override def itToUse = emptyIteration
    
      finish
    }
  }
  
  test("PopulationIteration will send a RequestSingles method to the provided MatchMaker and wait for a Singles "
      + "response. It will add any humans in the Singles Response to the population count and the population report "
      + "before shutting down.") {
    new DefaultTest {
      override def mockMM = (a: ActorRef) => Props(new ForwardActor(probe.ref))
      override def itToUse = emptyIteration
      
      actor.tell(Continue, worker.ref)
      
      probe.expectMsg(RequestSingles)
      
      actor ! Singles(Seq(manOfAge))
      
      probe.receiveOne(3 seconds) match {
        case Iteration(span, Accumulator(report :: Nil), population, _) =>
          assert(span === iteration.span)
          assert(report.count === 1)
          assert(population === manOfAge :: Nil)
      }
      
      deathProbe.expectTerminated(actor)
    }
  }
  
  test("PopulationIteration will not reduce the worker count if the sendMore in the message is set to false") {
    new DefaultTest {
      override def mockMM = (a: ActorRef) => Props(new ForwardActor(probe.ref))
      
      actor.tell(Ready, worker.ref)
      
      worker.receiveOne(3 seconds) match {
        case Work(Man(age), _) => assert(age === 15)
        case a: Any => fail("Expected Work(Man(15), actorRef), received " + a)
      }
      
      actor.tell(Count(readyForBaby, false), probe.ref)
      
      probe.expectNoMsg()
      
      actor.tell(Continue, worker.ref)
      
      probe.expectMsg(3 seconds, RequestSingles)
      
      finish      
    }
  }
  
  test("PopulationIteration should not die if it does not receive a Singles message in response to a SinglesRequest") {
    new DefaultTest {
      override def mockMM = (a: ActorRef) => Props(new ForwardActor(probe.ref))
      override def itToUse = emptyIteration
      
      actor.tell(Continue, worker.ref)
      
      probe.expectMsg(RequestSingles)
      
      deathProbe.expectNoMsg()
      
      finish
    }
  }
  
//  test("If the PopulationIteration sends a SinglesRequest, but then receives another message before receiving the "
//      + "singles request, it will start it's scan over and will need to send another SinglesRequest before it will "
//      + "shut down.") {
//    new NoScan {
//      override def mockMM = (a: ActorRef) => Props(new ForwardActor(probe.ref))
//      override def itToUse = emptyIteration
//      
//      actor ! Scan
//      
//      probe.expectMsg(RequestSingles)
//      
//      actor ! Continue
//      
//      probe.expectMsg(Done)
//      
//      actor ! Singles(Nil)
//      
//      probe.expectMsg(RequestSingles)
//      
//      actor ! Singles(Nil)
//      
//      deathProbe.expectTerminated(actor)
//    }
//  }
  
//  test("The working Receive PartialFunction should not return a Work object when it receives a scan") {
//    new NoScan {
//      actor ! Scan
//      probe.expectNoMsg()
//      
//      finish
//    }
//  }
//  
//  test("The empty Receive PartialFunction should not return a Done object when it receives a scan") {
//    new NoScan {
//      /* 
//       * Using an isolated probe as the default implicit probe is acting as the populationActor and will receive the 
//       * iteration response. We just want to make sure we don't receive a Done message.
//       */
//      override def itToUse = emptyIteration
//      val isolatedProbe = TestProbe()
//      actor.tell(Scan, isolatedProbe.ref)
//      isolatedProbe.expectNoMsg()
//      
//      finish
//    }
//  }
//  
  test("If the state of the PopulationIteration contains population when the Continue message is received, the "
      + "PopulationIteration will send work back to the sender.") {
    new DefaultTest {
      actor ! Continue
      
      probe.receiveOne(3 seconds) match {
        case Work(Man(age), _) => assert(age === 15)
        case any: Any => fail("Expected Work(Man(16), ActorRef), received " + any)
      }
      
      finish
    }
  }
  
//  test("If the state of the PopulationIteration does not contain population when the Continue message is received, the "
//      + "PopulationIteration will send a Done message to the sender") {
//    new DefaultTest {
//      override def itToUse = emptyIteration
//      
//      actor ! Continue
//      
////      probe.expectMsg(Done)
//      
//      finish
//    }
//  }
  
  test("If a Death is sent to the PopulationIteration, it will increment the death count by one in the report that is "
      + "sent to the PopulationActor") {
    new DefaultTest {
      override def itToUse = emptyIteration
      
      actor.tell(Death, worker.ref)
      
//      probe.expectMsg(Done)
      
      //actor ! Scan
      
      probe.receiveOne(3 seconds) match {
        case Iteration(1, Accumulator(PopulationSummary(0, 1, 0) :: Nil), Nil, timestamp) => assert(true)
        case any: Any => 
          fail("expected Iteration(0, Accumulator(Seq(PopulationSummary(0, 1, 0), Nil, timestamp), received " + any)
      }
      
      finish
    }
  }
  
  test("If a Birth is sent to the PopulationIteration, it will increment the birth count by one and update the "
      + "population count by three") {
    new DefaultTest {
      override def itToUse = emptyIteration
      
      actor.tell(Birth(readyForBaby, babyMan), worker.ref)
      
      probe.receiveOne(3 seconds) match {
        case Iteration(1, Accumulator(PopulationSummary(1, 0, 3) :: Nil), population, _) => {
          assert(population.contains(readyForBaby))
          assert(population.contains(babyMan))
        }
        case any: Any => 
          fail(
              "Expected "
                  + " Iteration("
                      + "0, "
                      + "Accumulator(Seq(PopulationSummary(1, 0, 3))), "
                      + "Seq(Husband(16, Woman(16), 0)), "
                      + "timestamp"
                      + "), "
                  + "received " + any
          )
      }
      
      finish
    }
  }
  
  test("If a Husband is sent in a Count to the PopulationIteration, the husband will be added to next year's "
      + "population and the count will be incremented by 2") {
    new DefaultTest {
      override def itToUse = emptyIteration
      
      actor.tell(Count(readyForBaby), worker.ref)
      
      probe.receiveOne(3 seconds) match {
        case Iteration(1, Accumulator(PopulationSummary(0, 0, 2) :: Nil), population, _) => 
          assert(population.contains(readyForBaby))
        case any: Any => 
          fail(
              "Expected "
                  + "Iterator("
                      + "0, "
                      + "Accumulator(PopulationSummary(0, 0, 2) :: Nil), "
                      + "Seq(Husband(16, Woman(16, 0)), _), "
                      + "timestamp"
                  + ")"
              + "recieved " + any
          )
      }
      
      finish
    }
  }
  
  test("If a Count message that doesn't contain a Husband is sent to the PopulationIteration actor it should increase "
      + "the population count by one") {
    new DefaultTest {
      override def itToUse = emptyIteration
      
      actor.tell(Count(womanOfAge), worker.ref)
      
      probe.receiveOne(3 seconds) match {
        case Iteration(1, Accumulator(PopulationSummary(0, 0, 1) :: Nil), population, _) => 
          assert(population.contains(womanOfAge))
        case any: Any => 
          fail("Expected Iterator(0, Accumulator(PopulationSummary(0, 0, 1) :: Nil), Seq(Woman(16, 0))), received " 
              + any
          )
      }
      finish
    }
  }
}
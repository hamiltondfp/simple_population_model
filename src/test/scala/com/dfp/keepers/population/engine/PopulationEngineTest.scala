package com.dfp.keepers.population.engine

import org.scalatest._
import com.dfp.keepers.population._
import Human._
import com.dfp.keepers.population.test.util.DefaultValues

class PopulationEngineTest extends FunSuite with DefaultValues {
  
  val engine = new PopulationEngine(100, testValues)
  
  test("Verify that a man that is over age will be recorded as dead") {
    val man = Man(age = testValues.lifeSpan)
    val report = engine.iterate(Seq(man))()
    
    val expected = PopulationReport(deaths = 1)
    
    assert(report === expected)
  }
  
  test("If a man comes of age and there is a woman of age, he will be paired with that woman") {
    val man = Man(age = testValues.mateAge)
    val woman = Woman(age = testValues.mateAge, 0)
    val initial = PopulationReport(count = 1, eligibleFemales = Seq(woman))
    
    val report = engine.iterate(Seq(man))(initial)
    
    val expected = PopulationReport(count = 2, population = Seq(Husband(man.age + 1, woman)))
    
    assert(report === expected)
  }
  
  test("If a man comes of age and there are no women of age, he will be added to the eligibleMales List") {
    val man = Man(age = testValues.mateAge)
    val report = engine.iterate(Seq(man))()
    
    val expected = PopulationReport(count = 1, eligibleMales = Seq(Man(man.age + 1)))
    
    assert(report === expected)
  }
  
  test("If a man is not of age, and does not die, he will simply increase in age") {
    val man = Man(age = testValues.mateAge - 1)
    val report = engine.iterate(Seq(man))()
    
    val expected = PopulationReport(count = 1, population = Seq(Man(age = man.age + 1)))
    
    assert(report === expected)
  }
  
  test("If a woman's age is equal to the expected lifespan, she will be recorded as dead") {
    val woman = Woman(age = testValues.lifeSpan, 0)
    val report = engine.iterate(Seq(woman))()
    
    val expected = PopulationReport(deaths = 1)
    
    assert(report === expected)
  }
  
  test("If a woman is of age and there is an eliglbe man of age, she will be paried with that man") {
    val woman = Woman(age = testValues.mateAge, 0)
    val man = Man(age = testValues.mateAge)
    val initial = PopulationReport(count = 1, eligibleMales = Seq(man))
    
    val report = engine.iterate(Seq(woman))(initial)
    
    val expected = PopulationReport(count = 2, population = Seq(Husband(man.age, Woman(woman.age + 1, 0))))
    
    assert(report === expected)
  }
  
  test("If a woman is of age, but is barren, she will not be added to the eligibleFemales list, but will instead only "
      + "grow older"
  ) {
    val woman = Woman(age = testValues.barren, 0)
    val report = engine.iterate(Seq(woman))()
    
    val expected = PopulationReport(count = 1, population = Seq(Woman(age = woman.age + 1, 0)))
    
    assert(report === expected)
  }
  
  test(
      "If a woman is of age and there are no eligible males of age, she will be added to the list of eligibleFemales"
  ) {
    val woman = Woman(age = testValues.mateAge, 0)
    val report = engine.iterate(Seq(woman))()
    
    val expected = PopulationReport(count = 1, eligibleFemales = Seq(Woman(age = woman.age + 1, 0)))
    
    assert(report === expected)
  }
  
  test("If a woman is not of age, and does not die, she will simply increase in age") {
    val woman = Woman(age = testValues.mateAge - 1, 0)
    val report = engine.iterate(Seq(woman))()
    
    val expected = PopulationReport(count = 1, population = Seq(Woman(woman.age + 1, 0)))
    
    assert(report === expected)
  }
  
  test("If a husband dies, his death will be recorded and his wife will be added to the population") {
    val woman = Woman(age = testValues.mateAge, 0)
    val man = Husband(age = testValues.lifeSpan, woman)
    val report = engine.iterate(Seq(man))()
    
    val expected = PopulationReport(deaths = 1, count = 1, population = Seq(Woman(woman.age + 1, 0)))
    
    assert(report === expected)
  }
  
  test("If wife dies, her death will be recorded and her husband will be added to the population") {
    val woman = Woman(age = testValues.lifeSpan, 0)
    val man = Husband(age = testValues.mateAge, woman)
    val report = engine.iterate(Seq(man))()
    
    val expected = PopulationReport(deaths = 1, count = 1, population = Seq(Man(man.age + 1)))
    
    assert(report === expected)
  }
  
  test("If woman has not had a child within the appropriate spacing between children, and is not barren, and is "
      + "attached to a husband, they will have a child"
  ) {
    val woman = Woman(age = testValues.mateAge, testValues.mateAge - testValues.childSpacing)
    val man = Husband(age = testValues.mateAge, woman)
    val report = engine.iterate(Seq(man))()
    
    assert(report.births === 1)
    assert(report.count === 3)
    assert(report.population.size === 2)
    assert(report.population.contains(Husband(man.age + 1, Woman(woman.age + 1, woman.age + 1))))
    
    //just to double check, since this one is all done manaually...
    assert(report.deaths === 0)
    assert(report.eligibleMales === Seq())
    assert(report.eligibleFemales === Seq())
  }
  
  test("If a woman's age minus her age when she last had a child is less than child spacing, and she is attached to a "
      + "husband, they will not have a child but will simply grow old together.") {
    val woman = Woman(age = testValues.mateAge, testValues.mateAge - (testValues.childSpacing - 1))
    val man = Husband(age = testValues.mateAge, woman)
    val report = engine.iterate(Seq(man))()
    
    val expected = PopulationReport(
        count = 2, 
        population = Seq(Husband(man.age + 1, Woman(woman.age + 1, woman.ageOfLastBirth)))
    )
    
    assert(report === expected)
  }
  
  test("If a woman is attached to a husband, and has not had a child within the appropriate spacing, but is barren, "
      + "then the couple will not reproduce and will simply grow old together.") {
    val woman = Woman(age = testValues.barren, testValues.barren - testValues.childSpacing)
    val man = Husband(age = testValues.mateAge, woman)
    val report = engine.iterate(Seq(man))()
    
    val expected = PopulationReport(
        count = 2, 
        population = Seq(Husband(man.age + 1, Woman(woman.age + 1, woman.ageOfLastBirth)))
    )
    
    assert(report === expected)
  }
}